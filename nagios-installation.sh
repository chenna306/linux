#!/bin/bash
echo "====Library packages installation====="
yum install gcc glibc glibc-common gd gd-devel make net-snmp openssl-devel xinetd unzip -y
yum install httpd -y

echo "=========User Creation============"
useradd nagios
groupadd nagcmd
usermod -a -G nagcmd nagios

sleep 2
echo "Changing Selinux"
setenforce 0

sleep 2
echo "=========Downoading package on /tmp/nagios==========="
mkdir /tmp/nagios
cd /tmp/nagios
curl -L -O https://assets.nagios.com/downloads/nagioscore/releases/nagios-4.1.1.tar.gz
sleep 2
tar xvf nagios-*.tar.gz

sleep 2
echo "========Installation of nagios========="
cd nagios-*
sleep 2
./configure --with-command-group=nagcmd
sleep 2
make all
make install
make install-commandmode
make install-init
make install-config
make install-webconf
usermod -G nagcmd apache

sleep 2
echo "==========Plugin Installation============"
cd /tmp/nagios
curl -L -O http://nagios-plugins.org/download/nagios-plugins-2.1.1.tar.gz
sleep 2
tar xvf nagios-plugins-*.tar.gz
sleep 2
cd nagios-plugins-*
./configure --with-nagios-user=nagios --with-nagios-group=nagios --with-openssl
make
make install

sleep 2
echo "=========Install NRPE==========="
cd /tmp/nagios
curl -L -O http://downloads.sourceforge.net/project/nagios/nrpe-2.x/nrpe-2.15/nrpe-2.15.tar.gz
tar xvf nrpe-*.tar.gz
cd nrpe-*
./configure --enable-command-args --with-nagios-user=nagios --with-nagios-group=nagios --with-ssl=/usr/bin/openssl --with-ssl-lib=/usr/lib/x86_64-linux-gnu
sleep 2
make all
make install
make install-xinetd
make install-daemon-config
sleep 2
echo "=========xinetd service restart============"
service xinetd restart
sleep 2
sleep 2
echo "============Remove # in /usr/local/nagios/etc/servers file =============="
sed -i '/cfg_dir/s/^#//g' /usr/local/nagios/etc/servers
mkdir /usr/local/nagios/etc/servers

sleep 2
echo "========Add the following to the end of the file:======"

echo "define command{" >> /usr/local/nagios/etc/objects/commands.cfg
echo "     command_name check_nrpe" >> /usr/local/nagios/etc/objects/commands.cfg
echo "     command_line $USER1$/check_nrpe -H $HOSTADDRESS$ -c $ARG1$" >> /usr/local/nagios/etc/objects/commands.cfg
echo "}" >> /usr/local/nagios/etc/objects/commands.cfg

############################################################################


echo "expect package installation"
yum install expect -y
############################################################################
echo "=======password Changing for nagiosadmin======="
expect -c "
spawn htpasswd -c /usr/local/nagios/etc/htpasswd.users nagiosadmin
expect ""
send "123456"\n
expect ""
send "123456"\n
expect ""
send \"exit\r\"
interact ";
sleep 4

############################################################################

echo "==========PHP packages installation=========="
wget http://mirror.centos.org/centos/7/os/x86_64/Packages/php-5.4.16-48.el7.x86_64.rpm
wget http://mirror.centos.org/centos/7/os/x86_64/Packages/libzip-0.10.1-8.el7.x86_64.rpm
wget http://mirror.centos.org/centos/7/os/x86_64/Packages/php-cli-5.4.16-48.el7.x86_64.rpm
wget http://mirror.centos.org/centos/7/os/x86_64/Packages/php-common-5.4.16-48.el7.x86_64.rpm

sleep 2
rpm -ivh *.rpm
sleep 2
echo "=========Service restart========"
systemctl daemon-reload
systemctl start nagios.service
systemctl restart httpd.service
chkconfig nagios on
systemctl restart nagios.service
systemctl restart httpd.service
systemctl stop firewalld
systemctl disabled firewalld

echo ""
echo -e "\e[1;31m Please update server-ip after 127.0.0.1 in /etc/xinetd.d/nrpe configuration file then restart xinetd restart \e[0m"
echo -e "\e[1;31m only_from = 127.0.0.1 IP addresss \e[0m"

echo "     "
echo -e "\e[1;32m Installation Completed \e[0m"
echo -e "\e[1;32m Try to access Nagios Connsole on browser: IP:/nagios \e[0m"
echo -e "\e[1;32m username: nagiosadmin, password 123456 \e[0m"
