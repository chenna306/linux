What is an Operating system?
An operating system is a software platform that creates an environment where a user can run different applications on a computing device. 
The operating system acts as a bridge between the software programs and the hardware components of a system.

Let's have a look at the components which are required for the functioning of an operating system.
The Bootloader: It takes care of the boot process of your device.
The Shell: A shell is a programming language which controls other files, processes, and controls all other programs as well.
The kernel: It is the main component of the OS and manages memory, CPU, and other related components.
Desktop Environment: Where a user interacts with.
Graphical server: It is a subsystem of OS and used for showing graphics on your screen
Applications: These are the set of programmes which perform different user tasks such as word, excel, etc.            
Daemons: Provider of background services.

What is Kernel?

Whenever an operating system is loaded into memory, firstly, the kernel is loaded and stays there till the operating system gets shut down. 
The kernel is responsible for taking care of low-level tasks such as task management, memory management, risk management, etc. 
The kernel is responsible for:

Process management for application execution.
Memory and I/O (input/output) management.
System call control (core act of Kernel).  
Device management with the help of device drivers

Linux operating system
Linux is an open-source platform developed in 1991 by Linus Torvalds. 
It is used in a wide variety of devices as an operating system, like in computers, servers, mobiles, mainframes, and other embedded devices. 
As it is open-source software, users can customize this operating system according to their needs. 
It supports almost every major computer platform such as ARM, x86, SPARK, etc. The most common usage of Linux is for Server, 
but it is also used in Desktop computers, ebook readers, smartphones, etc.

The Linux Kernel:

A Kernel is the core component of any Linux based operating system. It represents the core aspect of the Linux distributions for Desktop computers and servers.
It has a monolithic architecture, and the operating system operates entirely in the kernel space.
The monolithic kernel not only encircles the Central Processing Unit, IPC, and memory, 
but also has system server calls, device drivers, and file system management. 
Linux kernel works as a layer between the software and hardware of a device.


Operations of a Kernel:

The kernel is termed as a heart of any operating system as it controls all other programmes in a system. 
When a device starts, the kernel goes through a process called initialization function, such as checking memory. 
It takes care of the memory allocation part and creates an environment for running the applications without any disturbances.


Kernel works as a service provider, 
so the programs can request the kernel for accomplishing multiple tasks such as requesting the use of the disk, network card, or other pieces of hardware, 
and also, kernel sets interrupt for the CPU to enable multitasking. 
It protects the computational environment by not letting the faulty programmes enter into the operational functions of others. 
It stops the unauthorised programs at the entrance by not allowing the memory space and limits the CPU time that they consume. 


Types of kernels:
In general, we have three types of kernels, and they are,

Monolithic kernel: It contains many device drivers that create a communication interface between the hardware and software of a device.
Microkernel: It could only execute basic functionality.
Hybrid kernel: it combines the aspects of Monolithic kernel and Microkernel.

1.Monolithic kernel

It is a widely used kernel by the operating systems. 
In a Monolithic architecture, the kernel consists of various modules which can dynamically be loaded and unloaded.  
This kind of architecture would extend the capabilities of the OS and allows easy extensions to the kernel.

Maintenance of a kernel becomes easy with monolithic architecture because
it allows a concerned module to load and unload when there is a need to fix a bug in a particular module. 
So, it eliminates the tedious task of bringing down and recompiling the whole kernel for little changes. 
It is easier in the monolithic kernel to unload the module that is no more in usage.

2. MicroKernel
Microkernel has evolved as an alternative to the monolithic kernel to address the issue of the ever-growing size of kernel code which monolithic kernel failed to do. 
This architecture allows some basic services like protocol stack, device driver management, file system, etc., to run in userspace. 
This could enhance the capability of OS with minimum code, improved security, and ensures stability.

It restricts the damages to the impacted areas by leaving the rest of the system to function correctly without any interruptions. 
In Microkernel architecture, all the basic OS services are available to programmes via interprocess communication (IPC).  
Microkernel allows direct interaction between the device drivers and hardware.

3. Hybrid kernel
The hybrid kernel can decide what it wants to run in user mode, and in supervisor mode. 
In the hybrid kernel environment, usually, things like device drivers, file system I/O would run in user mode, 
whereas server calls and IPC are kept in supervisor mode. It gives the best experience of both the worlds.

Kernel space and Userspace?

In the Linux operating system, the system memory is divided into two different regions: kernel space and userspace. 
Let's look into each region of memory and know the functionalities of both.


1. Kernel space   
Kernel space is found in an elevated state which provides full access to the hardware devices and protects the memory space. 
This memory space and user space together called as Kernel-space. In a kernel space environment, 
core access to the system services and hardware are maintained and provided as a service to the rest of the system. 

2. User Space

The userspace or userland is a code which runs outside the operating system kernel environment.

Userspace is defined as various applications or programmes or libraries that an operating system uses to connect with the kernel. 
Because of the complicated process to access the memory, malicious functions can be restricted only to the user system.  



What is Linux ABI?
It is nothing but kernel userspace ABI (application binary user interface). 
It exists between program modules. ABIs are used to access the codes that are compiled and ready for usage. 
ABI is an interface between two binary program modules: 
one of these modules is an operating system facility or library, and the second one is a program run by a user.

The Linux Loadable Kernel Module:
If you want to add code to the Linux kernel, the first thing you need to do is to add some source files to the kernel source tree. 
There may be situations where you are required to add code to the kernels while it is running, this process is called a loadable kernel module.


The benefits of LKMs (Linux Loadable Kernel Module)

LKM saves time and avoids errors.
It helps in finding the bugs quickly.
LKMs save the memory because they are loaded into memory only when required.
It offers faster maintenance and debugging time.


Linux kernel interfaces:

The Linux Kernel provides different interfaces to the user-space applications that execute different tasks and have different properties.  
It consists of two separate Application Programming Interfaces (APIs): one is kernel userspace, and the other is kernel internal. 
Kernel user space is the Linux API userspace and allows the programs in the user space into system services and resources of the kernel.  

Major Subsystems of the Linux Kernel:
Below mentioned are the some of the subsystems of the Linux kernel. Let's discuss them one by one in detail.


System call interface:
A system call is a programmatic process in which a program requests a service from the kernel of an operating system. 
It includes various hardware services such as connecting with hardware devices and creating a communication interface among the integral parts of the Kernel. 
System call creates an efficient interface between an operating system and a process.

Process management:
The Kernel takes care of creating and destroying the different processes and monitors their connection to the outside world such as input and output.  
It handles the communication between different methods via signals, interprocess communication primitive, or pipes. 
In addition to all these, it also has a scheduler which controls the processes in sharing the CPU.  


1. Memory management:
Memory is a vital component of an Operating system and kernel takes care of it.  
Linux manages the available memory and hardware mechanisms for virtual and physical mappings.

Memory management isn't just managing 4KB buffers, and it is much more than that. 
Linux also provides abstractions other than 4kb buffers, known as a slab allocator.  
Slab allocator uses the 4kb buffer as its base but then allocates structures from inside by monitoring things like,
which pages are full, empty, and partially used. 
This allows the scheme to grow dynamically and in supporting the more significant needs of the system. 

2. Virtual file system:
Virtual file system (VFS) is an important integral part of the kernel and facilitates common interface abstraction for the file system. 
The VFS creates a switching layer between the file system supported by the kernel and SCI (System Call Interface).

In addition to the above things, Linux supports various types of file systems that require different ways of organizing data to store in physical format. 
For instance, a disk can be formatted with the commonly used FAT file system, or Linux standard ext3 file system, or several others.

3. Device Drivers:
A vast part of the source code of the kernel is stored in the device drivers, and that makes a specific hardware device usable. 
The Linux provides a driver subdirectory that is further divided into various devices that are supported, such as I2C, Bluetooth, serial, etc.


4. Architecture-dependent code:
Even though much of the Linux runs on its independent architecture, 
some elements should be considered for the architecture efficiency and normal operation. 
Linux has many subdirectories, and each architecture subdirectory has many numbers of other subsidiaries. 
And, these subdirectories focus on the specific tasks of the kernel such as memory management, boot, kernel, etc.


5. Upgrading the Kernel:
As we are aware of the concept called update, we do have that option in the kernel to update it from the older version to a newer one. 
The retention of old configurations is significant, and to achieve this, one has to back up the configuration file in the kernel source directory. 
If anything goes wrong while updating the kernel, follow the below steps.

Download the latest source code from the kernel.org main page.  
Apply variations to the old version tree to make it as a new one.
Reconfigure the kernel on the basis of the older kernel configuration file that you had backed up.
Develop the new kernel.
Now, you can install the latest kernel in your system.

Linux Kernel system (file system):
The files and systems in the Linux kernel system are where most of the users find difficulties, majorly because
it is hard to tell which files are in which directories if you don’t have the knowledge. 
For this reason, we shall try to look at the organization of the file systems. We shall also learn how to create, delete, move, and rename directories. 
Additionally, we shall learn how to edit files and change permissions.

The file system layout:
The UNIX file system can aptly fit into a one-line description; “Everything on a UNIX system that is not a process is a file”. 
This statement holds true for files that are a little bit more than just files. 
Therefore, a Linux system does not differentiate between a file and directory mainly because 
the directory, in essence, is a file containing names of other files, services, texts, images, and programs. 
Additionally, a Linux system also treats input and output devices as files. 
The general understanding is that the files are a sort of in a tree structure on the main hard drive; this is for easy management and order. 
Most of the files on a Linux system are regular files, regardless of the data they hold, be it programs, executable files or normal data.

While we have already said that everything in a Linux system is a file, there is a general understanding that there are some exceptions. For instance:

(a) Directories: A file list of other files

(b) Special files: These are the mechanisms used for input and output. Special files are in /dev.

(c) Links: This is a system to make a file, including directory visible in various parts of the “system tree”.

(d) Domain (sockets): These are special types of files similar to the IP/TCP sockets. 
These files are protected by the file system access control and they provide inter-process networking.

Named pipes: These types of files are the bridge between processes. 
They are more or less the same as sockets and enhance communication between processes without the use of networks or sockets semantics.

Remember that I had indicated that most computer users generalize that the file system is more or less like a tree, here is a good example of a Linux file system tree.

It is important to note that depending on the UNIX system in use, the file system tree may change; some files and directories may change.


Conclusion:
The Linux Kernel plays an important role in resource allocation to different applications.  
The kernel acts as a centralised place to connect the hardware and software and runs the applications in a system. 
Linux kernel has got more popularity due to its open-source nature. 
Users can customize this OS according to their requirements. Therefore, it’s been used by a wide variety of devices.  

The modular characteristic of the Linux kernel allows a wide range of modifications without rebooting the system. 
The  Flexibility of the kernel enables its users to perform their level best. 
Moreover, the monolithic nature of this kernel has greater computational power than the microkernel.
